<?php

// Подключаем автозагрузку классов composer-а
require_once '../vendor/autoload.php';

// Берем параметр, в который htaccess реврайтит набранный url
$route = filter_input(INPUT_GET, 'route');

// Удаляем последний слеш и разбиваем по слешам
$params = $route ? explode('/', trim($route, '/')) : [];

// Определяем запрошенный контроллер и экшн
$controller = 'PagesController';
$action = 'indexAction';
if ($params){
    $controller = ucfirst(array_shift($params)) . 'Controller';
    if ($params){
        $action = array_shift($params) . 'Action';
    }
}
$controller = 'App\\Controllers\\' . $controller;

// Если он существует, вызываем запрошенный экшн контроллера с остальными параметрами, иначе выводим экшн 404
if (method_exists($controller, $action)){
    (new $controller)->$action($params);
} else {
    (new \App\Controllers\PagesController)->notFoundAction($params);
}


