<?php

namespace App\Views;

class View {
    
    public function render($template, $arr = []){
        
        // Распаковываем переменные в текущую область видимости
        extract($arr);
        
        // Проверяем существование шаблона и подключаем его
        $fileName = '../app/templates/'. $template . '.php';
        if (file_exists($fileName)){
            require $fileName;
        }
        
    }

}
