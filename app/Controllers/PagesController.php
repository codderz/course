<?php

namespace App\Controllers;

use \App\Views\View;

class PagesController {
    
    public function indexAction(){
        (new View())->render('layout', [
            'content' => 'index'
        ]);
    }
    
    public function newsAction(){
        $model = new \App\Models\NewsModel();
        (new View())->render('layout', [
            'content' => 'news',
            'news' => $model->getAll()
        ]);    
    }
    
    public function aboutAction(){
        (new View())->render('layout', [
            'content' => 'about'
        ]);     
    }
    
    public function notFoundAction(){
        (new View())->render('layout', [
            'content' => '404'
        ]);     
    }
    
}
